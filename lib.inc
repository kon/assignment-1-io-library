%define EXIT_SYSCALL 60
%define WRITE_SYSCALL 1
%define STDOUT 1
%define NEW_LINE 0xA
%define TABULAR 0x9
%define SPACE 0x20
section .text 

; Принимает код возврата и завершает текущий процесс
global exit
exit: 
    mov  rax, EXIT_SYSCALL
    xor  rdi, rdi
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
global string_length
string_length:
    mov rax, rdi
    .next_char:
    mov dil, [rax]
    test dil, dil
    jz .return
    inc rdx
    inc rax
    jmp .next_char
    
    .return:
    mov rax, rdx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
global print_string
print_string:

    mov rax, rdi
    .next_char:
    mov dil, [rax]
    test dil, dil
    jz .return
    push rax
    call print_char
    pop rax
    inc rax
    jmp .next_char
    
    .return:
    ret

; Принимает код символа и выводит его в stdout
global print_char
print_char:
    push rdi; 
    mov rax, WRITE_SYSCALL
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
global print_newline
print_newline:
    mov rdi, NEW_LINE
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
global print_uint
print_uint:
    mov rax, rdi
    mov rsi, rsp
    
    xor rdx, rdx
    dec rsp
    mov [rsp], dl; save 00 

    mov r8, 10
    .next_digit:
    xor rdx, rdx
    idiv r8
    add rdx, 0x30

    dec rsp
    mov [rsp], dl; save result

    test rax, rax
    jz .end
    jmp .next_digit

    .end:
    mov rdi, rsp 
    push rsi
    call print_string
    
    .return:
    pop rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
global print_int
print_int:
    test rdi, rdi
    jns .pos
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .pos:
    jmp print_uint
    .return:
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
global string_equals
string_equals:
    xor rax, rax
    .next_char:
    mov r8, [rsi]
    mov rdx, [rdi]
    cmp r8b, dl
    jnz .return
    test r8b, r8b
    jz .yes
    inc rsi
    inc rdi
    jmp .next_char
    .yes:
    inc rax
    .return:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
global read_char
read_char:
    push 0
    xor rax, rax; read syscall number
    xor rdi, rdi; stdin file descriptor
    mov rsi, rsp
    mov rdx, 1; amount of bytes to read
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

global read_word
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi; r12 - start of buffer
    mov r13, rsi; r13 - capacity
    xor rax, rax
    xor r14, r14; counter
.skip_blank:
    call read_char
    cmp rax, TABULAR
    jz .skip_blank
    cmp rax, ' '
    jz .skip_blank
    cmp rax, NEW_LINE
    jz .skip_blank
    jmp .save_symb
.next_char:
    call read_char; char in rax
.save_symb:
    test rax, rax; check if ended
    jz .good
    cmp rax, TABULAR
    jz .good
    cmp rax, ' '
    jz .good
    cmp rax, NEW_LINE
    jz .good

    inc r14
    cmp r14, r13
    jz .bad
    
    mov [r12], al
    inc r12
    jmp .next_char

    .bad:
    xor rax, rax
    jmp .return
    .good:
    xor r13, r13
    mov [r12], r13
    sub r12, r14
    mov rax, r12
    mov rdx, r14

    .return:
    pop r14
    pop r13
    pop r12
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось


global parse_uint
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rsi, rsi
    mov r8, 10
    .next_digit:
    mov sil, [rdi]
    inc rdi

    cmp sil, '0'
    js .return
    cmp sil, '9'+1
    jns .return

    sub sil, '0'
    
    imul rax, r8

    add rax, rsi
    inc rdx

    
    jmp .next_digit


    .return:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
global parse_int
parse_int:
    xor rax, rax
    xor rdx, rdx
    mov rsi, [rdi]
    cmp sil, '-'
    jz .neg
    cmp sil, '+'
    jnz .pos
    inc rdi
    .pos:
    call parse_uint
    test rax, rax
    js .bad
    ret
    .neg:
    inc rdi
    call parse_uint
    test rax, rax
    js .bad
    mov rdi, rax
    neg rax
    test rdx, rdx
    jz .bad
    inc rdx
    ret
    .bad:
    xor rax, rax
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
global string_copy
string_copy:
    mov rcx, rdi
    mov rax, rdx
    .next_char:
    mov r8b, [rdi]
    mov [rsi], r8b
    test r8b, r8b

    jz .return
    inc rdi
    inc rsi
    dec rax
    
    test rax, rax
    jz .end
    jmp .next_char

    .end:
    xor rdx,rdx
    .return:
    mov rax, rdx
    ret
